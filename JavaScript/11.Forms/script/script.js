document.addEventListener('DOMContentLoaded',()=>{
    //1. Прив'яжіть усім інпутам наступну подію - втрата фокусу, кожен інпут виводить своє value в параграф з id="test"
  
    const inputs = document.querySelectorAll("input"); // получаем колекцию 
    const info = document.createElement('div'); // создаем блок для вывода инвормации 
    inputs.forEach((element) => { // перебираем колекцию 

         element.addEventListener("blur", (e) => { // добавляем слушатель 
        e.target.after(info); // добавляем блок инфо 
        info.innerHTML = ` <b> ${e.target.id}</b> : ${e.target.value}`;// заполняем 
      });
   })
/*
2. Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, 
то межа інпуту стає зеленою, якщо неправильна – червоною.

*/
inputs.forEach((element)=>{
   
element.addEventListener('blur',(e)=>{   
    const valueInput = e.target.value;    // сравниваем длинну значения и переобразовуем в строку , сравниваем на идентичность
     if(valueInput.length.toString() === element.dataset.length){
        element.classList.add('true-length')
        
     }else{
        element.classList.add('false-length')  
     }
})
})  
/*
3.Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅
Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 
*/
// получаем список городов
data.forEach(function (el){
    const option = document.createElement("option");
    option.innerText = el.city;
    document.querySelector("#city").append(option)
})

   let flagMain = false,
       flagName = false,
       flagSecondName = false,
       flagPhone = false,
       flagMail = false,
       flagCity =false,
       flagPrice =false;

    inputs.forEach((element)=>{ 
        element.addEventListener('blur',validateInput)// вызываем проверку
    })
     // проверка   регулярными выражением  
       function validateInput() {
        if (this.id === "first-name") {
            if (/^[А-яіїґє-]+$/.test(this.value)) {
                document.querySelector('#valid-name').innerText ="✅"
                flagName = true;
            }else{
                document.querySelector('#valid-name').innerText ="❌"
                flagName = false;
            } 
            console.log(flagName)   
        }if(this.id === "second-name"){
            if (/^[А-яіїґє-]+$/.test(this.value)) {
                document.querySelector('#valid-second-name').innerText ="✅"
               flagSecondName = true;
            }else{
                document.querySelector('#valid-second-name').innerText ="❌"
               flagSecondName = false;
            } 
            console.log(flagSecondName) ;
        }if(this.id === "phone"){ 
            if(/^\+? ?38\d{3} ?\d{3} ?\d{2} ?\d{2}$/.test(this.value)){
                document.querySelector('#valid-tel').innerText ="✅"
               flagPhone = true
            }else{
                document.querySelector('#valid-tel').innerText ="❌"
              flagSecondName = false;
            }
            console.log(flagPhone) ;
        }if(this.id === "e-mail"){
            if(/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/.test(this.value) ){
                document.querySelector('#valid-mail').innerText ="✅"
                flagMail = true;  
            }else{
                flagMail = false;
                document.querySelector('#valid-mail').innerText ="❌"
            }
            console.log(flagMail)
        }if(this.id === "price"){
            if(this.value > 0){
                flagPrice = true;
            }else{
                flagPrice = false;
            }
            console.log(flagPrice)
        }
    }
    const tel = document.querySelector('#phone');
      function checkOperator(){
        
        if(/050/.test(this.value) || /095/.test(this.value) ||  /066/.test(this.value) || /099/.test(this.value)){
         this.classList.add('vodafone')
        }else{
         this.classList.remove('vodafone')
        }
        if(/073/.test(this.value) || /093/.test(this.value)|| /063/.test(this.value)){
         this.classList.add('lifecell')
        }else{
         this.classList.remove('lifecell')
        }
        if(/067/.test(this.value) || /096/.test(this.value) || /097/.test(this.value)||/098/.test(this.value)){
         this.classList.add('kievstar')
        }else{
         this.classList.remove('kievstar')
        }
      }
      
  tel.addEventListener('input',checkOperator)

function checkFlag(){
    if(flagName === true && flagSecondName === true && flagPhone ===true && flagMail ===true && flagPrice ===true){
        flagMain = true
    }else{
        flagMain = false
    }
   console.log(flagMain)
}
 const form = document.querySelector('form');

 form.addEventListener('submit',formValidate )

 function formValidate(e){ // функция валидации формы
    e.preventDefault();
    checkFlag();
    if(flagMain !== true){
        alert("Fill in the form correctly")
    }else{
        alert("Great answers sent")
    }
 }
/*
 4.
 - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
 - Поведінка поля має бути такою:
 - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
 - Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
 
 Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
 - При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
 - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
 під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.*/
//создаем заполняем элементы
const price = document.querySelector('#price');
const  resBtn = document.createElement('span');
resBtn.innerText = '❌'
resBtn.style.marginLeft = '150px'
const spanEr = document.createElement('span');
const spanValue = document.createElement('span')
spanEr.innerText = 'Please enter correct price';

price.addEventListener('focus',(e)=>{ 
    price.style.border = '5px solid green'
})
price.addEventListener('focusout',(e)=>{
    if(price.value === ''){
        price.style.border = '';
        e.preventDefault()
     }else if (price.value < 0) {
        price.style.border = '5px solid red'
        price.insertAdjacentElement('beforebegin',spanEr)
        spanValue.remove()
        resBtn.remove()
     }
     else{
          price.style.border = '';
     spanValue.innerText = price.value;
     price.style.color = 'green'
     price.insertAdjacentElement('beforebegin',spanValue)
     spanValue.insertAdjacentElement('afterend',resBtn)
     spanEr.remove()
     }

})

//кнопка сброса
resBtn.addEventListener('click', ()=>{
    spanEr.remove();
    resBtn.remove();
    spanValue.remove
    price.value = '';
 })

})