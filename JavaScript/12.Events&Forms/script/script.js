document.addEventListener('DOMContentLoaded',()=>{
// Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и фамилии
/* class CreateUser{
    constructor(name,secondName){
     this.name = name;
     this.secondName = secondName;
    }
    show(){
       const divShow = document.createElement('div');
       divShow.innerText = `Name:${this.name} Second name:${this.secondName}`;
       document.body.append(divShow);
        
    }
}
const newUser = new CreateUser(prompt(`enter your first name`),prompt(`enter yor secondname`))
newUser.show();
saveData(newUser); */
// Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу синий фон, а 3 красный
const ul = document.createElement('ul');
const firstLi = document.createElement('li');
const secondLi = document.createElement('li');
const thirdLi = document.createElement('li');
const fourthLi = document.createElement('li');
firstLi.innerText = 'first ';
secondLi.innerText = 'second';
thirdLi.innerText = 'third';
fourthLi.innerText = 'fourth';
document.body.append(ul)
ul.append(firstLi);
firstLi.after(secondLi);
secondLi.after(thirdLi);
thirdLi.after(fourthLi);

document.querySelector('ul :nth-child(1)').style.background='blue';
document.querySelector('ul :nth-child(3)').style.background='red';
document.querySelector('ul :nth-child(1)').style.width='300px';
document.querySelector('ul :nth-child(3)').style.width='300px';

// Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом
  // координаты, где находится курсор мышки
  const div = document.createElement("div");
  div.style.width = div.style.height =  "300px";
  div.style.background ="#e9c46a";
  div.style.margin = "20px auto";
  div.style.padding = "20px";
  ul.insertAdjacentElement("afterend", div);
  const x = document.createElement("p");
  const y= document.createElement("p");
  div.prepend(x);
  div.append(y);
  div.addEventListener('mousemove',showCoordinates)
  function showCoordinates(e) {
    x.textContent = ` x : ${e.offsetX}`;
    y.textContent = ` y:  ${e.offsetY}`;
  }
  // Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата
const firstBtn =document.createElement('input');
firstBtn.setAttribute('type','button');
firstBtn.setAttribute('value','first button');

const secondBtn =document.createElement('input');
secondBtn.setAttribute('type','button');
secondBtn.setAttribute('value','second button');

const thirdBtn =document.createElement('input');
thirdBtn.setAttribute('type','button');
thirdBtn.setAttribute('value','third  button');

const fourthBtn =document.createElement('input');
fourthBtn.setAttribute('type','button');
fourthBtn.setAttribute('value','fourth  button');

div.insertAdjacentElement("afterend",firstBtn);
firstBtn.after(secondBtn);
secondBtn.after(thirdBtn);
thirdBtn.after(fourthBtn);

const divBtnInfo = document.createElement('div');
fourthBtn.after(divBtnInfo);

const btns = document.querySelectorAll("[type='button']");
btns.forEach(el => {
    el.addEventListener("click", (e) => {
      divBtnInfo.innerText=`you click : ${e.target.value}`;
    });
  });

  // Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице
  
  const moveDiv = document.createElement('div');
divBtnInfo.after(moveDiv);
  moveDiv.style.width = moveDiv.style.height =  "100px";
  moveDiv.style.background = '#ffb703';
  moveDiv.innerText = ' hover on me ';
  
  moveDiv.addEventListener('mousemove',(e)=>{
    
  
      moveDiv.style.position = `relative`
      moveDiv.style.bottom = Math.floor(Math.random() *100) + `px`
      moveDiv.style.left = Math.floor(Math.random() *100) + `px`
  
  })
  

  // Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body
  const color = document.createElement('input');
  color.setAttribute("type","color");
  moveDiv.after(color)
  color.style.marginTop = '50px'
  color.addEventListener('input', (e)=>{
    document.body.style.background = color.value
  })

  // Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль
  
  const login = document.createElement('input');
  login.style.display = 'block'
  login.style.marginTop = '50px'
  login.setAttribute('placeholder','enter your login');
  login.setAttribute('type','text');
  document.body.append(login);
  login.addEventListener('input', () => {
    console.log(login.value)
  })
 // Создайте поле для ввода данных поле введения данных выведите текст под полем
  const inputComenth = document.createElement('input');
  inputComenth.style.marginTop ='50px' 
  inputComenth.style.display = 'block'
  inputComenth.setAttribute('placeholder','enter something')
  login.after(inputComenth)
  const  comenthInfo = document.createElement('div');
  inputComenth.after(comenthInfo)
  inputComenth.addEventListener('change', (e) =>{
     comenthInfo.innerText =`your things : ${inputComenth.value}` ;
    })
})