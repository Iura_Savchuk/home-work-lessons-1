document.addEventListener('DOMContentLoaded',()=>{
/*
 Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":

    https://jsonplaceholder.typicode.com/comments?_page=5 
    https://jsonplaceholder.typicode.com/comments?_page=5&_limit=10
*/
	
async function getData() { // создаем асинхронную функцию запрос
    const response = await fetch('https://jsonplaceholder.typicode.com/comments'); // запрос
    const data = await response.json(); // переобразовуем ответ
    return data;
    
}
 
  // основная функция
  async function main() {
    const comenthData = await getData(); // массив с ответами 
    let currentPage = 1; // текущая страница
    let rows = 10;    // количество элементов которые будут выведены  на страницу
    if(getData()) document.querySelector('.loader').style.display = "none";// убираю лоадер
  
    function displayList(arrData, rowPerPage, page) {  // 
      const comenthEl = document.querySelector('.comenths');
      comenthEl.innerHTML = "";   // очищаем предыдущую страницу при клике
      page--; // приходит первая , вместо второй
     

      const start = rowPerPage * page;// старт для слайса массмва
      const end = start + rowPerPage;// конец слайса массива 
      const paginatedData = arrData.slice(start, end);
  
      paginatedData.forEach((el) => {
                                          // создаем и заполняем элементы , добавляем классы
        const comCard = document.createElement('div'),
               idCom = document.createElement('p'),
               nameCom = document.createElement('p'),
               mailCom = document.createElement('p'),
               bodyCom = document.createElement('div');
               idCom.innerText = `id : ${el.id}`;
               nameCom.innerText = `name : ${el.name}`;
               mailCom.innerText = ` e-mail : ${el.email}`;
               bodyCom.innerText = ` body : ${el.body}`;
               comenthEl.appendChild(comCard);
               comCard.appendChild(idCom);
               comCard.appendChild(nameCom);
               comCard.appendChild(mailCom);
               comCard.appendChild(bodyCom);
               comCard.classList.add('comenth-card');
               idCom.classList.add('id-comenth');
               nameCom.classList.add('name-comenth');
               mailCom.classList.add('mail-comenth');
               bodyCom.classList.add('body-comenth');
       
      })
    }
  
    function displayPagination(arrData, rowPerPage) {
      const paginationEl = document.querySelector('.pagination');
      const pagesCount = Math.ceil(arrData.length / rowPerPage);// количество кнопок пагинации
      const ulEl = document.createElement("ul");
      ulEl.classList.add('pagination__list');
      

      for (let i = 0; i < pagesCount; i++) {  // создаем и добавляем кнопки
        const liEl = displayPaginationBtn(i + 1);
        ulEl.appendChild(liEl);
      }

      paginationEl.appendChild(ulEl);
      
    }
      
    function displayPaginationBtn(page) {
        
      const liEl = document.createElement("li");
      liEl.classList.add('pagination__item')
      liEl.innerText = page                  
  
      if (currentPage == page) liEl.classList.add('pagination__item--active');
  
      liEl.addEventListener('click', () => {   // делаем кнопки кликабельными 
        currentPage = page
        displayList(comenthData , rows, currentPage)
  
        let currentItemLi = document.querySelector('li.pagination__item--active');
        currentItemLi.classList.remove('pagination__item--active'); // удаляем активный клас с предыдущей кнопки
  
        liEl.classList.add('pagination__item--active');//добавляем текущей
      })
        
      return liEl;
    }
  
    displayList(comenthData , rows, currentPage);
    displayPagination(comenthData , rows);
  }
  
  main();

})