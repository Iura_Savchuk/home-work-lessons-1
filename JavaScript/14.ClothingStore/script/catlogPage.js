document.addEventListener('DOMContentLoaded',()=>{
  const pagination = document.querySelector('.pagination');
  const sizeBtn = document.querySelector('.filter-cont');
  
  sizeBtn.addEventListener('click', (e) => {
    let filtredData = [];
      if (e.target.value === 'all') {
          filtredData = productData;
      }
      else if (e.target.value === 's') {
          filtredData = productData.filter(el => el.size === 's');
      }
      else if (e.target.value === 'm') {
          filtredData = productData.filter(el => el.size === 'm');
      }
      else if (e.target.value === 'l') {
          filtredData = productData.filter(el => el.size === 'l');
      }
      else if (e.target.value === 'xl') {
          filtredData = productData.filter(el => el.size === 'xl');
      }
      else if (e.target.value === '-100') {
          filtredData = productData.filter(el => el.price < 100);
      }
      else if (e.target.value === '100+') {
          filtredData = productData.filter(el => el.price > 100 && el.price < 200);
      }
      else if (e.target.value === '200+') {
          filtredData = productData.filter(el => el.price > 200);
      }
      pagination.innerHTML = ''
      main(filtredData);

  })
   if (!sizeBtn.target){
    main(productData);
   }
})





