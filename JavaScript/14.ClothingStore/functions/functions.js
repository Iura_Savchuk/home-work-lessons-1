function main(data) {

    let currentPage = 1; // текущая страница
    let rows = 9;    // количество элементов которые будут выведены  на страницу

    function displayList(arrData, rowPerPage, page) {  // 
        const prodCont = document.querySelector('.list__conatainer');
        prodCont.innerHTML = "";   // очищаем предыдущую страницу при клике
        page--; // приходит первая , вместо второй

        const start = rowPerPage * page;// старт для слайса массмва
        const end = start + rowPerPage;// конец слайса массива 
        const paginatedData = arrData.slice(start, end);

        paginatedData.map((item) => {

            const prodItem = document.createElement('li');
            const prodCard = document.createElement('div');
            const prodName = document.createElement('p');
            const prodImg = document.createElement('img');
            const prodPrice = document.createElement('p');
            const addBtn = document.createElement('button');
            prodCont.append(prodItem);
            prodItem.append(prodCard);
            prodItem.append(addBtn);
            prodCard.append(prodImg);
            prodCard.append(prodName);
            prodCard.append(prodPrice);
            prodImg.setAttribute('src', item.img);
            prodImg.setAttribute('alt', item.name);
            prodName.innerText = item.name;
            prodPrice.innerText = `Price : ${item.price} $`;
            addBtn.innerText = 'add to cart';
            prodItem.classList.add('list__item');
            prodImg.classList.add('product__photo');
            prodName.classList.add('product-name');
            prodPrice.classList.add('product-price');
            addBtn.classList.add('prod-btn')
            const cartName = item.name;
            const cartPrice= item.price;
            const cartImg= item.img;
            const itemDescription = item.description;
            const itemSize = item.size;
            
            prodCard.addEventListener('click',()=>{
                console.log(cartPrice)
                const modal = document.createElement('div');
                modal.classList.add('modal');
                const modalContent = document.createElement('div');
                modalContent.classList.add('modal-content');
                const closeBtn = document.createElement('button');
                closeBtn.innerText = "❌"
                closeBtn.classList.add('close-btn');
                const modalItem = document.createElement('div');
                modalItem.classList.add('modal-item');
                const modalImg = document.createElement('img');
                modalImg.setAttribute('src',cartImg);
                modalImg.setAttribute('alt',cartName);
                modalImg.classList.add('modal-img');
                let modalName = document.createElement('p');
                modalName.classList.add('modal-name');
                const modalPrice = document.createElement('p');
                modalPrice.classList.add('modal-price');
                const modalSize = document.createElement('p');
                modalSize.classList.add('modal-size');
                const addModalBtn = document.createElement('button');
                addModalBtn.classList.add('modal-btn');
                addModalBtn.innerText = 'add to bag';
                const descriptionTitle = document.createElement('div');
                descriptionTitle.classList.add('description-title');
                descriptionTitle.innerText = 'DETAILS'
                const modalDescription = document.createElement('div');
                modalDescription.classList.add('modal-description');
                const modalinfo = document.createElement('div');
                modal.style.display = 'block'

                modalName.textContent = cartName;
                modalPrice.textContent = `${cartPrice} $`;
                modalSize.textContent = `Size : ${itemSize}`;
                modalDescription.textContent = itemDescription;

                document.body.append(modal);
                modal.append(modalContent);
                modalContent.append(closeBtn);
                closeBtn.addEventListener('click',()=>{modal.remove()})
                modalContent.append(modalItem);
                modalItem.append(modalImg);
                modalItem.append(modalinfo);
                modalinfo.append(modalName);
                modalinfo.append(modalPrice);
                modalinfo.append(modalSize);
                modalinfo.append(addModalBtn);
                modalContent.append(descriptionTitle);
                modalContent.append(modalDescription);
                addModalBtn.addEventListener('click', ()=>{
                    const cartStorage = localStorage.getItem('cart') || '[]'
                const cart = JSON.parse(cartStorage);
                const card = {cartName,cartImg,cartPrice};
                localStorage.setItem('cart',JSON.stringify([...cart,card]))
                })


            })

            addBtn.addEventListener('click',()=>{
                console.log(item.name)
                const cartStorage = localStorage.getItem('cart') || '[]'
                const cart = JSON.parse(cartStorage);
                const card = {cartName,cartImg,cartPrice};
                localStorage.setItem('cart',JSON.stringify([...cart,card]))
            })


        })
    }

    function displayPagination(arrData, rowPerPage) {
        const paginationEl = document.querySelector('.pagination');
        const pagesCount = Math.ceil(arrData.length / rowPerPage);// количество кнопок пагинации
        const ulEl = document.createElement("div");
        ulEl.classList.add('pagination__list');


        for (let i = 0; i < pagesCount; i++) {  // создаем и добавляем кнопки
            const liEl = displayPaginationBtn(i + 1);
            ulEl.appendChild(liEl);
        }

        paginationEl.appendChild(ulEl);

    }

    function displayPaginationBtn(page) {

        const liEl = document.createElement("button");
        liEl.classList.add('pagination__item')
        liEl.innerText = page

        if (currentPage == page) liEl.classList.add('pagination__item--active');

        liEl.addEventListener('click', () => {   // делаем кнопки кликабельными 
            currentPage = page
            displayList(data, rows, currentPage)

            let currentItemLi = document.querySelector('button.pagination__item--active');
            currentItemLi.classList.remove('pagination__item--active'); // удаляем активный клас с предыдущей кнопки

            liEl.classList.add('pagination__item--active');//добавляем текущей
        })

        return liEl;
    }

    displayList(data, rows, currentPage);
    displayPagination(data, rows);
}


  
       



 

    







