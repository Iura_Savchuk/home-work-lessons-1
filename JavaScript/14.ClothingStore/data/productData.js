

   const productData =[
    {
      id:1,
      name:'Icon G Essential Sweat Hoodie',
      img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw841f2ce9/pim/202203/2037000/93/202203-2037000-93-flat-fv-1.jpg?sw=200',
      price:90,
      size:'s',
      description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
      stretch cargo shorts are practically the uniform on yachts worldwide!
       A customer favorite for over 50 years, they're called the world's best boating shorts.
        Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },

    {
        id:2,
        name:'Monogram Merino V-Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw6c65ccc5/pim/202203/8060044/274/202203-8060044-274-flat-fv-1.jpg?sw=200',
        price:156,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
      
    },
    {
        id:3,
        name:'Icon G Essential Crew Neck Sweatshirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw6f84a015/pim/202203/2036000/93/202203-2036000-93-flat-fv-1.jpg?sw=200',
        price:84,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:4 ,
        name:'Crest Shield Block Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw1088f244/pim/202203/2037027/433/202203-2037027-433-flat-fv-1.jpg?sw=200',
        price:90,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:5 ,
        name:'Original Piqué Polo Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw793c04c1/pim/202203/2201/369/202203-2201-369-flat-fv-1.jpg?sw=200',
        price:60,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:6 ,
        name:'Regular Fit Broadcloth Tartan Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwe2b50af3/pim/202203/3220067/650/202203-3220067-650-flat-fv-1.jpg?sw=200',
        price:72,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:7 ,
        name:'Brushed Wool Crew Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw87a9275d/pim/202203/8060027/335/202203-8060027-335-flat-fv-1.jpg?sw=200',
        price:108,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:8 ,
        name:'Lambswool Cable Crew Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw416dda81/pim/202203/8050123/678/202203-8050123-678-flat-fv-1.jpg?sw=200',
        price:108,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:9 ,
        name:'Wool Harrington Jacket',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwe92ddd9e/pim/202203/7006257/433/202203-7006257-433-flat-fv-1.jpg?sw=200',
        price:285,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:10 ,
        name:'Oversized Wool Coat',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwbd6aad01/pim/202203/7006261/213/202203-7006261-213-flat-fv-1.jpg?sw=200',
        price:465,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:11 ,
        name:'Paisley Tie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw22214722/pim/202203/9950121/276/202203-9950121-276-detail-fv-1.jpg?sw=200',
        price:56,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:12 ,
        name:'GANT x Wrangler MWZ Jeans',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwc96ce706/pim/202203/1000227/982/202203-1000227-982-model-fv-1.jpg?sw=200',
        price:105,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:13 ,
        name:' Slim Fit Silk Western Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw420a1bf8/pim/202203/3220114/130/202203-3220114-130-flat-fv-1.jpg?sw=200',
        price:215,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:14 ,
        name:'Regular Fit Flannel Plaid Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw754eda9d/pim/202203/3220079/464/202203-3220079-464-flat-fv-1.jpg?sw=200',
        price:80,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:15 ,
        name:'Allister Regular Fit Twill Chinos',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw0a885007/pim/202204/1500039/248/202204-1500039-248-flat-fv-1.jpg?sw=200',
        price:130,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:16 ,
        name:'Contrast Collar Piqué Rugger',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwfa16e637/pim/202204/2052003/5/202204-2052003-5-flat-fv-1.jpg?sw=200',
        price:105,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:17 ,
        name:'Original Piqué Polo Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw93c1ab85/pim/202203/2201/604/202203-2201-604-flat-fv-1.jpg?sw=200',
        price:65,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:18 ,
        name:'Leather GANT Varsity Jacket',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwb629c564/pim/202203/7006249/36/202203-7006249-36-flat-fv-1.jpg?sw=200',
        price:690,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:19 ,
        name:'Light Down Gilet',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwe467ec1b/pim/202204/4700210/5/202204-4700210-5-flat-fv-1.jpg?sw=200',
        price:140,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:20 ,
        name:'Monogram Crew Neck Sweatshirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw00049c7b/pim/202203/2036023/433/202203-2036023-433-flat-fv-1.jpg?sw=200',
        price:84,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:21 ,
        name:'Original Piqué Polo Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwff6af8f4/pim/202203/2201/620/202203-2201-620-flat-fv-1.jpg?sw=200',
        price:100,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:22 ,
        name:'Reversible Bomber Jacket',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw2dc0cc73/pim/202203/7006245/369/202203-7006245-369-flat-fv-1.jpg?sw=200',
        price:255,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:23 ,
        name:'Original Slim Fit T-Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw0e830ffd/pim/202204/234102/94/202204-234102-94-flat-fv-1.jpg?sw=200',
        price:45,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:24 ,
        name:'Quilted Flannel Overshirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw6bf07016/pim/202203/3220076/433/202203-3220076-433-flat-fv-1.jpg?sw=200',
        price:150,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:25 ,
        name:'Archive Shield Crew Neck Sweatshirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw94572d7b/pim/202204/2046071/93/202204-2046071-93-flat-fv-1.jpg?sw=200',
        price:140,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:26 ,
        name:'Crest Shield Sweat Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw23c6128d/pim/202203/2037028/630/202203-2037028-630-flat-fv-1.jpg?sw=200',
        price:85,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:27 ,
        name:'Archive Shield Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw15b3f874/pim/202204/2047056/113/202204-2047056-113-flat-fv-1.jpg?sw=200',
        price:150,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:28 ,
        name:'Crest Shield Sweat Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwd453432f/pim/202203/2037028/93/202203-2037028-93-flat-fv-1.jpg?sw=200',
        price:84,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:29 ,
        name:'Archive Shield Sweatpants',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw98ba0770/pim/202204/2049005/5/202204-2049005-5-flat-fv-1.jpg?sw=200',
        price:130,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:30 ,
        name:'Archive Shield T-Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw5e824a65/pim/202203/2003099/363/202203-2003099-363-flat-fv-1.jpg?sw=200',
        price:28,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:31 ,
        name:'Barstripe Archive Shield T-Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw02dc69ff/pim/202203/2003149/93/202203-2003149-93-flat-fv-1.jpg?sw=200',
        price:28,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:32 ,
        name:'Regular Fit Gingham Broadcloth Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw6ebf8d32/pim/202203/3046700/822/202203-3046700-822-flat-fv-1.jpg?sw=200',
        price:72,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:33 ,
        name:'Cotton Piqué Half-Zip Cardigan',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw154b8fd8/pim/202204/8030524/92/202204-8030524-92-flat-fv-1.jpg?sw=200',
        price:85,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:34 ,
        name:'Jogger Chinos',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw09ccf6d6/pim/202203/1505160/410/202203-1505160-410-flat-fv-1.jpg?sw=200',
        price:85,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:35 ,
        name:'Relaxed Fit Tattersall Oxford Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw9d9057b5/pim/202203/3220029/130/202203-3220029-130-flat-fv-1.jpg?sw=200',
        price:72,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:36 ,
        name:'Regular Fit Broadcloth Tartan Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwd2837899/pim/202203/3220067/338/202203-3220067-338-flat-fv-1.jpg?sw=200',
        price:72,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:37 ,
        name:'Light Down Jacket',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwc5d9a894/pim/202204/7006298/301/202204-7006298-301-flat-fv-1.jpg?sw=200',
        price:300,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:38 ,
        name:'Sailing Tracksuit Pants',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw3f471302/pim/202204/2004039/433/202204-2004039-433-flat-fv-1.jpg?sw=200',
        price:156,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:39 ,
        name:'Regular Fit Plaid Flannel Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw67148894/pim/202204/3230004/630/202204-3230004-630-flat-fv-1.jpg?sw=200',
        price:140,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:40 ,
        name:'Banner Shield Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw35ab4abf/pim/202204/2007039/630/202204-2007039-630-flat-fv-1.jpg?sw=200',
        price:150,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:41 ,
        name:'Banner Shield Crew Neck Sweatshirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwaefa69c9/pim/202204/2006049/93/202204-2006049-93-flat-fv-1.jpg?sw=200',
        price:140,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:42 ,
        name:'Original Piqué Polo Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw62ea565f/pim/202204/2201/256/202204-2201-256-flat-fv-1.jpg?sw=200',
        price:105,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:43 ,
        name:'Maritime Hoodie',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw61cfd2a5/pim/202204/2036032/433/202204-2036032-433-flat-fv-1.jpg?sw=200',
        price:175,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:44 ,
        name:'Cotton Cable Crew Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw3a1f363a/pim/202204/8050501/93/202204-8050501-93-flat-fv-1.jpg?sw=200',
        price:165,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:45 ,
        name:'Super Fine Lambswool V-Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw2d21fceb/pim/202203/86212/480/202203-86212-480-flat-fv-1.jpg?sw=200',
        price:99,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:46 ,
        name:'Hallden Slim Fit Twill Chinos',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw14842e29/pim/202204/1500038/410/202204-1500038-410-flat-fv-1.jpg?sw=200',
        price:130,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:47 ,
        name:'Hallden Slim Fit Tech Prep™ Chinos',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw87f3402d/pim/202204/1501356/410/202204-1501356-410-flat-fv-1.jpg?sw=200',
        price:165,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:48 ,
        name:'Oversized Denim Twill Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw51644014/pim/202203/3220028/971/202203-3220028-971-flat-fv-1.jpg?sw=200',
        price:99,
        size:'m',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:49 ,
        name:'Flight Puffer Jacket',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw9f193f82/pim/202203/7006277/19/202203-7006277-19-flat-fv-1.jpg?sw=200',
        price:225,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:50 ,
        name:'Relaxed Fit Rodeo Leather Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwf14bf89c/pim/202203/3220051/5/202203-3220051-5-flat-fv-1.jpg?sw=200',
        price:411,
        size:'s',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:51 ,
        name:'Classic Cotton V-Neck Sweater',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw98ca86cc/pim/202204/8030552/92/202204-8030552-92-flat-fv-1.jpg?sw=200',
        price:130,
        size:'xl',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:52 ,
        name:'Regular Fit Gingham Broadcloth Shirt',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dw76a9e4a8/pim/202204/3046700/436/202204-3046700-436-flat-fv-1.jpg?sw=200',
        price:120,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },
    {
        id:53 ,
        name:'Hallden Slim Fit Twill Chinos',
        img:'https://gant.com/dw/image/v2/BFLN_PRD/on/demandware.static/-/Sites-gant-master/default/dwea536f79/pim/202203/1500038/5/202203-1500038-5-flat-fv-1.jpg?sw=200',
        price:130,
        size:'l',
        description: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' 
        stretch cargo shorts are practically the uniform on yachts worldwide!
         A customer favorite for over 50 years, they're called the world's best boating shorts.
          Featuring a 4 inch inseam for sizes 30-40 and 5.5 inch inseam for sizes 42-46. `
    },


]



