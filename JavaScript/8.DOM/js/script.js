// функция выполняется по событию (загрузка страницы)
window.onload = () => {
    //получаем переменную с кнопкой
    const btn = document.querySelector("#btn");
    // функция по событию (клик)
    btn.onclick = () => {
        //создание строки ввода , кнопки и атрибуттов 
        const inp = document.createElement("input");
        const createBtn = document.createElement("input");
        inp.placeholder="enter diametr circle (px)";
        createBtn.type= "button";
        createBtn.value= "draw";
        btn.replaceWith(inp);//меняем местами с первоначальной кнопкой
        inp.after(createBtn);//вставляем новую кнопку 
        //создание кругов по клику 
        createBtn.onclick = () =>{
            const circleCont = document.createElement("div");
            circleCont.classList.add("circle-cont");
            circleCont.style.width = `${10*inp.value}px`;// размер контейнера
            document.body.append(circleCont)
           // createBtn.after(circleCont);
            // цикл для создания кругов
            for(i=0;i<100;i++){
                const circle = document.createElement("div");
                circle.classList.add("circle");
                circle.style.width = inp.value + "px";
                circle.style.height = inp.value + "px";
                circle.style.backgroundColor = ranCol();
                circle.style.borderColor = ranCol();
                circleCont.prepend(circle);
                // удаление по клику 
                circle.onclick = (j) =>{
                    j.target.remove()
                }

            }
            // выбор случайного цвета 
            function ranCol(){
                const ranCol =Math.floor(Math.random()*360);
                return `hsl(${ranCol}, 50%, 50%)`//
            }


        }
    } 




























}