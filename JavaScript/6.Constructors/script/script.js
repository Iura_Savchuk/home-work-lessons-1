/*Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
              
read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.
*/
function Calculator() {

    this.read = function() {
       this.firstNum = +prompt(`Enter firs number`) ;
       this.secondNum = +prompt(`Enter second number`) ;
    };
    
    this.sum = function() {
      return  this.firstNum + this.secondNum;
    };
    
    this.mul = function() {
  return this.firstNum * this.secondNum ;
    };
    this.sub = function () {
      return this.firstNum - this.secondNum ;
    };
    this.div = function () {
       return this.firstNum / this.secondNum ;
    };
    
    }
    
    let calculator = new Calculator();
    calculator.read();
    
    
    document.write(`<b>Sum of entered numbers :</b> ${calculator.sum()}<br> `);
    document.write(`<b>Multiplication of entered numbers :</b> ${calculator.mul()} <br>`);
    document.write(`<b>Subtraction of entered numbers :</b> ${calculator.sub()} <br>`);
    document.write(`<b>Division of entered numbers :</b> ${calculator.div()} <br>`);