//создаем константы , находим элементы
const findEl =(sel) =>document.querySelector(sel),
elHour  = findEl(".stopwatch-display :nth-child(1)"),
elMinute= findEl(".stopwatch-display :nth-child(2)"), 
elSecond= findEl(".stopwatch-display :nth-child(3)"),
elMsec= findEl(".stopwatch-display :nth-child(4)"),
elStart = findEl(".stopwatch-control :nth-child(1)"),
elPause = findEl(".stopwatch-control :nth-child(2)"),
elStop = findEl(".stopwatch-control :nth-child(3)"),
elNew = findEl(".stopwatch-control :nth-child(4)"),
watchCont = findEl(".container-stopwatch");
//создаем переменные 
let hour = 00,
    minute = 00,
    second = 00,
    msec = 00,
    count = 00,
    interval;
 

//старт
elStart.onclick =()=>{
    clearInterval(interval);
    interval=setInterval(startTimer,10);
    //изменение цвета
    watchCont.classList.add("green");
    watchCont.classList.remove("silver");
    watchCont.classList.remove("red");
    watchCont.classList.remove("black");
}
//пауза
elPause.onclick = ()=>{
    clearInterval(interval);
    //изменение цвета
    watchCont.classList.add("silver");
    watchCont.classList.remove("green");
    watchCont.classList.remove("red");
    watchCont.classList.remove("black");
}
//стоп
elStop.onclick= ()=>{
    clearInterval(interval);
  clear();
  //изменение цвета
  watchCont.classList.add("red");
  watchCont.classList.remove("green");
    watchCont.classList.remove("silver");
    watchCont.classList.remove("black");
}
//сброс
function clear() {
    hour = 00;
    minute = 00;
    second = 00;
    msec = 00;
    elHour.textContent ='00';
    elMinute.textContent ='00' ;
    elSecond.textContent ='00';
    elMsec.textContent ='00';

}
//новый круг
elNew.onclick=()=>{
    clearInterval(interval)
    count ++
    const result = document.querySelector(".result")
    const blockRes = document.createElement('div')
    blockRes.innerText =`Lap   ${count} - :${hour}:${minute}:${second}:${msec}`
    result.append(blockRes)
    clear()
    interval=setInterval(startTimer,10)
    //изменение цвета
    watchCont.classList.add("green");
    watchCont.classList.remove("silver");
    watchCont.classList.remove("red");
    watchCont.classList.remove("black");
}
//функция - таймер
function startTimer(){
    //милисекунды
    msec ++;
    if(msec<9){
        elMsec.innerText =`0${msec}` ; 
    }if(msec>9){
        elMsec.innerText = msec;
    }
     if(msec>99) {
        second++;
        elSecond.innerText = `0${second}`;
       msec=0;
       elMsec.innerText =`0${msec}` ;
    
}
//секунды
if(second<9){
    elSecond.innerText =`0${second}`;
}if(second>9){
    elSecond.innerText = second
}if(second>59){
    minute++;
    elMinute.innerText =`0${minute}`;
    second = 0;
    elSecond.innerText =`0${second}`;
}
//минуты
if(minute<9){
    elMinute.innerText =`0${minute}`;
}if(minute>9){
    elMinute.innerText =minute;
}if(minute>59){
    hour++;
    elHour.innerText=`0${hour}`;
    minute = 0;
    elMinute.innerText =`0${minute}`;

}
//часы 
if(hour<9){
    elHour.innerText =`0${hour}`;
}if(hour>9){
    elHour.innerText=hour;
}
}