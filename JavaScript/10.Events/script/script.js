window.addEventListener('DOMContentLoaded',()=>{
// добавляем константы , переменные 
const btns = document.querySelector('.keys'),
      resulytBtn = document.querySelector('.orange'),
      display = document.querySelector('.display > input'),
      mString = document.createElement('span');
      //добавляем м
      mString.classList.add('m-string');
      document.querySelector(".display").append(mString);
      let firstValue='' ,
      secondValue='',
      operation='',
      count = 0,
      memory='',
      result='',
      flag = false;
      // функция-клик , добавляем логику кнопкам 
      btns.addEventListener('click',(e)=>{
        //присвоение и вывод выбраных значения 
        if(e.target.classList.contains('black')){
            let num = e.target.value;
            //первое
            if(secondValue ==="" && operation ===""){
                firstValue += num;
                showValue(firstValue,display);
                //второе
            }else if (firstValue !=='' && operation !=='' && flag){
                secondValue = num;
                flag = false; 
                showValue(firstValue, display);
                }else{                
                secondValue += num;
                showValue(secondValue, display);
            }
            //активация =
            if (secondValue !== '') {
                resulytBtn.removeAttribute('disabled');
                
            return;
            }
        }
       ////сброс
        if (e.target.classList.contains("c")) {
            firstValue = '';
            secondValue = '';
            operation = '';
            flag = false;
            resulytBtn.setAttribute('disabled', '');
            showValue('', display);
        }
        //операция 
        if (e.target.classList.contains("pink")) {
            let newOp = e.target.value;
            operation = newOp;
            
        }
        //  =
        if (e.target.classList.contains("orange")) {
            if (secondValue === '') secondValue = firstValue;
            calc();
            showValue(result, display);
        }
        //  m
        if (e.target.classList.contains("gray")) {
            // m+
                        if (e.target.value === "m+") {
                            mString.textContent = ('m');
            
                            if (secondValue === '') memory = +secondValue;
                            else {
                                calc();
                                memory = (+memory) + (+result);
                            }
                            firstValue = '';
                            secondValue = '';
                            operation = '';
                            flag = false;
                            resulytBtn.setAttribute('disabled', '');
                            showValue(result, display);
                        }
            // m -
                        if (e.target.value === "m-") {
                            mString.innerText = "m";
                            if (secondValue === '') memory -= firstValue;
                            else {
                                calc();
                                memory = memory - result;
                            }
                            firstValue = '';
                            secondValue = '';
                            operation = '';
                            flag = false;
                            resulytBtn.setAttribute('disabled', '');
                        }
            //mrs
                        if (e.target.value === "mrc") {
                            display.value = memory;
                            count++;
                            if (count === 2) {
                                memory = 0;
                                display.value = 0;
                                mString.textContent = "";
                                count = 0;
                               
                            }
                        }
            
                    }
      })
//вывод на дисплей
function showValue(value, dp) {
    dp.value = value;
}
   
      // +
    let plus = (firstValue = 0, secondValue = 0) => {
        return (+firstValue) + (+secondValue);
    }
// -
    let minus = (firstValue = 0, secondValue = 0) => {
        return firstValue - secondValue;
    }
// *
    let multiplication = (firstValue = 0, secondValue = 0) => {
        return firstValue * secondValue;
    }
// /
    let division = (firstValue = 0, secondValue = 0) => {
        if (secondValue == 0) {
            return 'error';
        } else {
            return firstValue / secondValue;
        }
    }
// Функция подсчета
    function calculate(firstValue, secondValue, func ) {
        result = func(firstValue, secondValue);
    }
    function calc() {
        if (secondValue === '') secondValue = firstValue;
        switch (operation) {
            case '+':
                calculate(firstValue, secondValue, plus);
                break;

            case "*":
                calculate(firstValue, secondValue, multiplication);
                break;

            case "-":
                calculate(firstValue, secondValue, minus);
                break;

            case "/":
                calculate(firstValue, secondValue, division);
                break;

        }
        flag = true;
    }


})